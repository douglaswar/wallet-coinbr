angular.module('greenWalletMercadoControllers',
    ['greenWalletServices'])
.controller('MercadoController', ['$rootScope','$scope','$http' ,'wallets', 'tx_sender', 'notices', 'cordovaReady', 'hostname', 'gaEvent', '$modal', '$location', 'qrcode', 'clipboard',
        function MercadoController($rootScope, $scope, $http, wallets, tx_sender, notices, cordovaReady, hostname, gaEvent, $modal, $location, qrcode, clipboard) {
   if(!wallets.requireWallet($scope)) return;
   
    $rootScope.mostraLoading = 1;        
    $scope.atualizaValorBTC = function(){
        $scope.isBTC = true;
        $scope.atualizaTabelaVenda();
    }

    $scope.atualizaValorReal = function(){
        $scope.isBTC = false;
        $scope.atualizaTabelaVenda();
    }

    $scope.atualizaTabelaVenda = function(){
            
        if($scope.numeroExchanges == 6){
            $rootScope.mostraLoading = 0;
            $scope.BRL = 0;
            if($scope.isBTC) {
                $scope.Real = $rootScope.coinBR.valorReal * $scope.BTC;
            } else {
                $scope.BTC = $scope.Real / $rootScope.coinBR.valorReal;
            }
            for(i=0; i<$rootScope.valoresMercado.length; i++){ 
                $scope.BTCatual = 0;
                $scope.precoAtual = 0;
                if($rootScope.valoresMercado[i].nome == "Basebit"){
                    $scope.baseBit = 1;
                    $scope.baseBitCompra = 1;
                }
                if($rootScope.valoresMercado[i].nome == "coinBR"){
                    continue;
                }
                for(j=0; j<$rootScope.valoresMercado[i].asks.length; j++){
                    if($scope.baseBit == 1){
                        if($scope.BTCatual + parseFloat($rootScope.valoresMercado[i].asks[j].quantity) < $scope.BTC){
                            $scope.BTCatual += parseFloat($rootScope.valoresMercado[i].asks[j].quantity);
                            $scope.precoAtual += parseFloat($rootScope.valoresMercado[i].asks[j].price) * parseFloat($rootScope.valoresMercado[i].asks[j].quantity);
                        }
                        else if($scope.BTCatual + parseFloat($rootScope.valoresMercado[i].asks[j].quantity) > $scope.BTC){
                            $scope.valorAdquirido = $scope.BTC - $scope.BTCatual;
                            $scope.BTCatual += $scope.valorAdquirido;
                            $scope.precoAtual += $scope.valorAdquirido * parseFloat($rootScope.valoresMercado[i].asks[j].price);
                            break;
                        }
                        else if($scope.BTCatual + parseFloat($rootScope.valoresMercado[i].asks[j].quantity) == $scope.BTC){
                            $scope.BTCatual += parseFloat($rootScope.valoresMercado[i].asks[j].quantity);
                            $scope.precoAtual += parseFloat($rootScope.valoresMercado[i].asks[j].price) * parseFloat($rootScope.valoresMercado[i].asks[j].quantity);
                            break;
                        }    

                    }else{
                        if($scope.BTCatual + $rootScope.valoresMercado[i].asks[j][1] < $scope.BTC){
                            $scope.BTCatual += $rootScope.valoresMercado[i].asks[j][1];
                            $scope.precoAtual += $rootScope.valoresMercado[i].asks[j][0] * $rootScope.valoresMercado[i].asks[j][1];
                        }
                        else if($scope.BTCatual + $rootScope.valoresMercado[i].asks[j][1] > $scope.BTC){
                            $scope.valorAdquirido = $scope.BTC - $scope.BTCatual;
                            $scope.BTCatual += $scope.valorAdquirido;
                            $scope.precoAtual += $scope.valorAdquirido * $rootScope.valoresMercado[i].asks[j][0];
                            break;
                        }
                        else if($scope.BTCatual + $rootScope.valoresMercado[i].asks[j][1] == $scope.BTC){
                            $scope.BTCatual += $rootScope.valoresMercado[i].asks[j][1];
                            $scope.precoAtual += $rootScope.valoresMercado[i].asks[j][0] * $rootScope.valoresMercado[i].asks[j][1];
                            break;
                        }    
                    }               
                }

                $scope.baseBit = 0;
                if($scope.BTCatual == $scope.BTC){
                    $rootScope.valoresMercado[i].possuiQtdVenda = 1;
                    $rootScope.valoresMercado[i].valorMedio =  $scope.precoAtual/$scope.BTC;
                    $rootScope.valoresMercado[i].valorReal =  (($scope.precoAtual * (100+$rootScope.valoresMercado[i].taxas))/100)/$scope.BTC;
                    if($scope.isBTC) {
                        $rootScope.valoresMercado[i].valorRealFinal =  ($scope.precoAtual * (100+$rootScope.valoresMercado[i].taxas))/100;
                        $rootScope.valoresMercado[i].valorRealFinal = $rootScope.valoresMercado[i].valorRealFinal.formatMoney(2,',','.');
                    } else {
                        $rootScope.valoresMercado[i].valorRealFinal = $scope.Real / $rootScope.valoresMercado[i].valorReal;
                        $rootScope.valoresMercado[i].valorRealFinal = (Math.round(parseFloat($rootScope.valoresMercado[i].valorRealFinal) * Math.pow(10, 8)) / Math.pow(10, 8));
                        if(isNaN($rootScope.valoresMercado[i].valorRealFinal))
                            $rootScope.valoresMercado[i].valorRealFinal = 0;
                    }
                }
                else{
                    $rootScope.valoresMercado[i].possuiQtdVenda = 0;
                    $rootScope.valoresMercado[i].valorReal =  "Não há";
                    $rootScope.valoresMercado[i].valorRealFinal =  "Não há";
                }

                $scope.BTCatualVenda = 0;
                $scope.precoAtualVenda = 0;
                for(j=0; j<$rootScope.valoresMercado[i].bids.length; j++){
                    if($scope.baseBitCompra == 1){
                        if($scope.BTCatualVenda + parseFloat($rootScope.valoresMercado[i].bids[j].quantity) < $scope.BTC){
                            $scope.BTCatualVenda += parseFloat($rootScope.valoresMercado[i].bids[j].quantity);
                            $scope.precoAtualVenda += parseFloat($rootScope.valoresMercado[i].bids[j].price) * parseFloat($rootScope.valoresMercado[i].bids[j].quantity);
                        }
                        else if($scope.BTCatualVenda + parseFloat($rootScope.valoresMercado[i].bids[j].quantity) > $scope.BTC){
                            $scope.valorAdquiridoVenda = $scope.BTC - $scope.BTCatualVenda;
                            $scope.BTCatualVenda += $scope.valorAdquiridoVenda;
                            $scope.precoAtualVenda += $scope.valorAdquiridoVenda * parseFloat($rootScope.valoresMercado[i].bids[j].price);
                            break;
                        }
                        else if($scope.BTCatualVenda + parseFloat($rootScope.valoresMercado[i].bids[j].quantity) == $scope.BTC){
                            $scope.BTCatualVenda += parseFloat($rootScope.valoresMercado[i].bids[j].quantity);
                            $scope.precoAtualVenda += parseFloat($rootScope.valoresMercado[i].bids[j].price) * parseFloat($rootScope.valoresMercado[i].bids[j].quantity);
                            break;
                        }    

                    }else{
                        if($scope.BTCatualVenda + $rootScope.valoresMercado[i].bids[j][1] < $scope.BTC){
                            $scope.BTCatualVenda += $rootScope.valoresMercado[i].bids[j][1];
                            $scope.precoAtualVenda += $rootScope.valoresMercado[i].bids[j][0] * $rootScope.valoresMercado[i].bids[j][1];
                        }
                        else if($scope.BTCatualVenda + $rootScope.valoresMercado[i].bids[j][1] > $scope.BTC){
                            $scope.valorAdquiridoVenda = $scope.BTC - $scope.BTCatualVenda;
                            $scope.BTCatualVenda += $scope.valorAdquiridoVenda;
                            $scope.precoAtualVenda += $scope.valorAdquiridoVenda * $rootScope.valoresMercado[i].bids[j][0];
                            break;
                        }
                        else if($scope.BTCatualVenda + $rootScope.valoresMercado[i].bids[j][1] == $scope.BTC){
                            $scope.BTCatualVenda += $rootScope.valoresMercado[i].bids[j][1];
                            $scope.precoAtualVenda += $rootScope.valoresMercado[i].bids[j][0] * $rootScope.valoresMercado[i].bids[j][1];
                            break;
                        }    
                    }               
                }
                $scope.baseBitCompra = 0;
                if($scope.BTCatualVenda == $scope.BTC){
                    if($scope.isBTC) {
                        $rootScope.valoresMercado[i].possuiQtdCompra = 1;
                        $rootScope.valoresMercado[i].valorRealVenda =  (($scope.precoAtualVenda * (100-$rootScope.valoresMercado[i].taxasVenda))/100)/$scope.BTC;
                        $rootScope.valoresMercado[i].valorRealFinalVenda =  ($scope.precoAtualVenda * (100-$rootScope.valoresMercado[i].taxasVenda))/100;
                        $rootScope.valoresMercado[i].valorRealFinalVenda = $rootScope.valoresMercado[i].valorRealFinalVenda.formatMoney(2,',','.');
                    } else {
                        $rootScope.valoresMercado[i].valorRealFinalVenda = "Não há";
                    }
                }else{
                    $rootScope.valoresMercado[i].possuiQtdCompra = 0;
                    $rootScope.valoresMercado[i].valorRealVenda =  "Não há";
                    $rootScope.valoresMercado[i].valorRealFinalVenda =  "Não há";
                }
                
                
            }
            if($rootScope.valoresMercado.length == 6){
                 $rootScope.valoresMercado.splice(0,1);
            }
            $rootScope.valoresMercado.sort(function(a,b){return a.valorReal - b.valorReal});
            
            if($scope.isBTC) {
                $rootScope.coinBR.valorRealFinal = $rootScope.coinBR.valorReal * $scope.BTC;
                $rootScope.coinBR.valorRealFinal = $rootScope.coinBR.valorRealFinal.formatMoney(2,',','.');
                $rootScope.coinBR.valorRealFinalVenda = $rootScope.coinBR.valorRealVenda * $scope.BTC;
                $rootScope.coinBR.valorRealFinalVenda = $rootScope.coinBR.valorRealFinalVenda.formatMoney(2,',','.');
                $scope.Real = $rootScope.coinBR.valorRealFinal;
            } else {
                $rootScope.coinBR.valorRealFinal = $scope.Real / $rootScope.coinBR.valorReal;
                $rootScope.coinBR.valorRealFinal = (Math.round($rootScope.coinBR.valorRealFinal * Math.pow(10, 8)) / Math.pow(10, 8));
                $rootScope.coinBR.valorRealFinalVenda = "Não há";
                $scope.BTC = $rootScope.coinBR.valorRealFinal;
                if(isNaN($rootScope.coinBR.valorRealFinal))
                    $rootScope.coinBR.valorRealFinal = 0;
            }
            $rootScope.valoresMercado.unshift($rootScope.coinBR); 
        }
    }

    $rootScope.counterMercado++;

    if(!wallets.requireWallet($scope)) return;
    $scope.mercado = {valorBTC : ""};
    $rootScope.valoresMercado = [];
    $rootScope.valoresMercadoVenda = [];
    $scope.BTC = 1.00000000;
    $scope.isBTC = true;
    $scope.numeroExchanges = 0;

    $http.get("https://api.blinktrade.com/api/v1/BRL/orderbook")
    .success(function(data) {
        a = {};
        aVenda = {};
        a.nome = "Foxbit";
        a.taxas = 1.24;
        a.taxasVenda = 1.24;
        a.valorMedio = 0;
        a.valorReal = 0;
        a.valorRealFinal = 0;
        a.valorRealFinalVenda = 0;
        a.asks = data.asks;
        a.bids = data.bids;
        a.possuiQtdVenda = 1;
        a.possuiQtdCompra = 1;


        aVenda.valorRealVenda = 0;        
        
        
        $rootScope.valoresMercado.push(a);
        $rootScope.valoresMercadoVenda.push(aVenda);
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
    })
    .error(function(data, status) {
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
        console.log('ERRO');
    });

    $http.get("https://www.mercadobitcoin.net/api/orderbook/")
   .success(function(data) {
        b = {};
        bVenda = {};
        b.nome = "Mercado Bitcoin";
        b.taxas = 2.69;
        b.taxasVenda = 2.29;
        b.valorMedio = 0;
        b.valorReal = 0;
        b.valorRealFinal = 0;
        b.valorRealFinalVenda = 0;
        b.asks = data.asks;
        b.bids = data.bids;
        b.possuiQtdVenda = 1;
        b.possuiQtdCompra = 1;

        bVenda.nome = "Mercado Bitcoin";
        bVenda.valorRealVenda = 0;
                

        $rootScope.valoresMercado.push(b);
        $rootScope.valoresMercadoVenda.push(bVenda);
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
    })
    .error(function(data, status) {
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
        console.log('ERRO');
  });

    $http.get("https://www.bitcointoyou.com/API/orderbook.aspx")
   .success(function(data) {
        c = {};
        cVenda = {};
        c.nome = "Bitcoin2u";
        c.taxas = 2.49;
        c.taxasVenda = 2.14;
        c.valorMedio = 0;
        c.valorReal = 0;
        c.valorRealFinal = 0;
        c.valorRealFinalVenda = 0;
        c.asks = data.asks;
        c.bids = data.bids;
        c.possuiQtdVenda = 1;
        c.possuiQtdCompra = 1;


        cVenda.nome = "Bitcoin2u";
        cVenda.valorRealVenda = 0;
        
        

        $rootScope.valoresMercado.push(c);
        $rootScope.valoresMercadoVenda.push(cVenda);
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
    })
    .error(function(data, status) {
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
        console.log('ERRO');
  });

    /*$http.get("https://api.bitinvest.com.br/exchange/orderbook?subscription-key=6d3230b4891c4a6ab7ec57b3dec5f0b8")
   .success(function(data) {
        e = {};
        eVenda = {};
        e.nome = "Bitinvest";
        e.taxas = 1.29;
        e.taxasVenda = 1.29;
        e.valorMedio = 0;
        e.valorReal = 0;
        e.valorRealFinal = 0;
        e.valorRealFinalVenda = 0;
        e.asks = data.asks;
        e.bids = data.bids;
        e.possuiQtdVenda = 1;
        e.possuiQtdCompra = 1;

        eVenda.nome = "Bitinvest";
        eVenda.valorRealVenda = 0;
        
        eVenda.bids = data.bids;
        

        $rootScope.valoresMercado.push(e);
        $rootScope.valoresMercadoVenda.push(eVenda);
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
    })
    .error(function(data, status) {
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
        console.log('ERRO');
  });

    $http.get("http://www.basebit.com.br/book-BTC_BRL")
   .success(function(data) {
        d = {};
        dVenda = {};
        d.nome = "Basebit";
        d.taxas = 2.09;
        d.taxasVenda = 1.74;
        d.valorMedio = 0;
        d.valorReal = 0;
        d.valorRealFinal = 0;
        d.valorRealFinalVenda = 0;
        d.asks = data.result.asks;
        d.bids = data.result.bids;
        d.possuiQtdVenda = 1;
        d.possuiQtdCompra = 1;

        dVenda.nome = "Basebit";
        dVenda.valorRealVenda = 0;
        
        dVenda.bids = data.result.bids;

        $rootScope.valoresMercado.push(d);
        $rootScope.valoresMercadoVenda.push(dVenda);
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
    })
    .error(function(data, status) {
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
        console.log('ERRO');
  });*/

  $http.get("http://www.negociecoins.com.br/api/v3/btcbrl/orderbook")
   .success(function(data) {
        d = {};
        dVenda = {};
        d.nome = "NegocieCoins";
        d.taxas = 0.30;
        d.taxasVenda = 0.20;
        d.valorMedio = 0;
        d.valorReal = 0;
        d.valorRealFinal = 0;
        d.valorRealFinalVenda = 0;
        d.asks = data.ask.map(function(value, index) { return [value.price,value.quantity]; });
        d.bids = data.bid.map(function(value, index) { return [value.price,value.quantity]; });
        d.possuiQtdVenda = 1;
        d.possuiQtdCompra = 1;

        dVenda.nome = "NegocieCoins";
        dVenda.valorRealVenda = 0;
        
        dVenda.bids = data.bid.map(function(value, index) { return [value.price,value.quantity]; });

        $rootScope.valoresMercado.push(d);
        $rootScope.valoresMercadoVenda.push(dVenda);
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
    })
    .error(function(data, status) {
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
        console.log('ERRO');
  });

  $http.get("https://api.flowbtc.com:8400/GetOrderBook/BTCBRL/")
   .success(function(data) {
        e = {};
        eVenda = {};
        e.nome = "FlowBTC";
        e.taxas = 1.24;
        e.taxasVenda = 0.85;
        e.valorMedio = 0;
        e.valorReal = 0;
        e.valorRealFinal = 0;
        e.valorRealFinalVenda = 0;
        e.asks = data.asks;
        e.bids = data.bids;
        e.possuiQtdVenda = 1;
        e.possuiQtdCompra = 1;

        eVenda.nome = "FlowBTC";
        eVenda.valorRealVenda = 0;
        
        eVenda.bids = data.bids;
        

        $rootScope.valoresMercado.push(e);
        $rootScope.valoresMercadoVenda.push(eVenda);
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
    })
    .error(function(data, status) {
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
        console.log('ERRO');
  });
    

    $http.get("https://www.coinbr.net/pubticker/")
   .success(function(data) {
        $rootScope.coinBR.nome = "coinBR";
        $rootScope.coinBR.taxas = 0;
        $rootScope.coinBR.valorMedio = 0;
        $rootScope.coinBR.valorReal = data.sell;
        $rootScope.coinBR.valorRealVenda = data.buy;
        $rootScope.coinBR.valorRealFinal = data.sell;
        $rootScope.coinBR.valorRealFinalVenda = data.buy;
        $rootScope.coinBR.precoFixoCompra = data.sell;
        $rootScope.coinBR.precoFixoVenda = data.buy;  
        $rootScope.coinBR.precoFixoVendaPadraoBr = data.buy;
        $rootScope.coinBR.precoFixoVendaPadraoBr = parseFloat($rootScope.coinBR.precoFixoVendaPadraoBr).formatMoney(2,',','.');
        $scope.Real = parseFloat($rootScope.coinBR.valorRealFinal).formatMoney(2,',','.')
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
    })
    .error(function(data, status) {
        $scope.numeroExchanges++;
        $scope.atualizaTabelaVenda();
        console.log('ERRO');
  });
   

}]);
